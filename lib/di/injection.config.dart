// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:weather_app/data/datasources/datasources.dart' as _i8;
import 'package:weather_app/data/datasources/remote/api_clients/weather_api_client.dart'
    as _i5;
import 'package:weather_app/data/datasources/remote/weather_service.dart'
    as _i6;
import 'package:weather_app/data/repositories/storage_repo.dart' as _i4;
import 'package:weather_app/data/repositories/weather_repo.dart' as _i7;
import 'package:weather_app/domain/domain.dart' as _i3;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.factory<_i3.StorageRepo>(() => _i4.StorageRepoImpl());
    gh.lazySingleton<_i5.WeatherApiClient>(() => _i5.WeatherApiClient());
    gh.lazySingleton<_i6.WeatherService>(
        () => _i6.WeatherService(gh<_i5.WeatherApiClient>()));
    gh.factory<_i3.WeatherRepo>(
        () => _i7.WeatherRepoImpl(gh<_i8.WeatherService>()));
    return this;
  }
}
