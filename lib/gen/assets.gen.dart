/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/ico_01.png
  AssetGenImage get ico01 => const AssetGenImage('assets/images/ico_01.png');

  /// File path: assets/images/ico_02.png
  AssetGenImage get ico02 => const AssetGenImage('assets/images/ico_02.png');

  /// File path: assets/images/ico_03.png
  AssetGenImage get ico03 => const AssetGenImage('assets/images/ico_03.png');

  /// File path: assets/images/ico_04.png
  AssetGenImage get ico04 => const AssetGenImage('assets/images/ico_04.png');

  /// File path: assets/images/ico_09.png
  AssetGenImage get ico09 => const AssetGenImage('assets/images/ico_09.png');

  /// File path: assets/images/ico_10.png
  AssetGenImage get ico10 => const AssetGenImage('assets/images/ico_10.png');

  /// File path: assets/images/ico_11.png
  AssetGenImage get ico11 => const AssetGenImage('assets/images/ico_11.png');

  /// File path: assets/images/ico_13.png
  AssetGenImage get ico13 => const AssetGenImage('assets/images/ico_13.png');

  /// File path: assets/images/ico_50.png
  AssetGenImage get ico50 => const AssetGenImage('assets/images/ico_50.png');

  /// File path: assets/images/ico_sunrise.png
  AssetGenImage get icoSunrise =>
      const AssetGenImage('assets/images/ico_sunrise.png');

  /// File path: assets/images/ico_sunset.png
  AssetGenImage get icoSunset =>
      const AssetGenImage('assets/images/ico_sunset.png');

  /// File path: assets/images/ico_temp-max.png
  AssetGenImage get icoTempMax =>
      const AssetGenImage('assets/images/ico_temp-max.png');

  /// File path: assets/images/ico_temp-min.png
  AssetGenImage get icoTempMin =>
      const AssetGenImage('assets/images/ico_temp-min.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        ico01,
        ico02,
        ico03,
        ico04,
        ico09,
        ico10,
        ico11,
        ico13,
        ico50,
        icoSunrise,
        icoSunset,
        icoTempMax,
        icoTempMin
      ];
}

class $AssetsJsonGen {
  const $AssetsJsonGen();

  /// File path: assets/json/vn.json
  String get vn => 'assets/json/vn.json';

  /// List of all assets
  List<String> get values => [vn];
}

class $AssetsSvgGen {
  const $AssetsSvgGen();

  /// File path: assets/svg/ico_gps.svg
  SvgGenImage get icoGps => const SvgGenImage('assets/svg/ico_gps.svg');

  /// List of all assets
  List<SvgGenImage> get values => [icoGps];
}

class Assets {
  Assets._();

  static const $AssetsImagesGen images = $AssetsImagesGen();
  static const $AssetsJsonGen json = $AssetsJsonGen();
  static const $AssetsSvgGen svg = $AssetsSvgGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter,
      color: color,
      colorBlendMode: colorBlendMode,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
