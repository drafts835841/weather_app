import '../models/models.dart';
import '../repositories/repositories.dart';

class WeatherUsecase {
  static const _apiKey = 'e47ca5aaa67cc2039025388a150b716b';

  const WeatherUsecase(this._weatherRepo);

  final WeatherRepo _weatherRepo;

  Future<CurrentWeatherResponse?> fetchData(LatLng? latLng) {
    if (latLng == null) return Future.value(null);
    return _weatherRepo.fetchWeather(_apiKey, latLng);
  }
}
