import '../models/models.dart';
import '../repositories/storage_repo.dart';

class SplashUsecase {
  const SplashUsecase(this._storageRepo);

  final StorageRepo _storageRepo;

  Future<void> init() async {
    final isInitialized = await _storageRepo.getValue(SharedPrefKey.initialized.key);
    if (isInitialized != null && isInitialized is bool) return;

    await Future.wait([_storageRepo.cleanUp(), _storageRepo.cleanUpSecure()]);
    await _storageRepo.setValue(SharedPrefKey.initialized.key, true);
  }
}
