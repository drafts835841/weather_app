import '../models/models.dart';
import '../repositories/storage_repo.dart';

class DetectLocationUsecase {
  const DetectLocationUsecase(this._storageRepo);

  final StorageRepo _storageRepo;

  Future<LatLng?> get defaultLatLng async {
    final result = await _storageRepo.getValue(
      SharedPrefKey.latestPosition.key,
      isSecure: SharedPrefKey.latestPosition.isSecure,
    );
    return LatLng.fromString(result.toString());
  }

  Future<String?> get defaultCityName async {
    return await _storageRepo.getValue(
      SharedPrefKey.latestCityName.key,
      isSecure: SharedPrefKey.latestCityName.isSecure,
    );
  }

  Future<dynamic> setDefaultLatLng(LatLng latLng) => _storageRepo.setSecureValue(
        SharedPrefKey.latestPosition.key,
        latLng.toString(),
      );

  Future<void> setDefaultCityName(String? cityName) async {
    if (cityName?.trim().isNotEmpty ?? false) {
      await _storageRepo.setValue(SharedPrefKey.latestCityName.key, cityName);
    }
  }
}
