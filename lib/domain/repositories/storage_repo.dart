abstract class StorageRepo {
  Future<Map<String, String>> readAll();

  Future<dynamic> getValue(String key, {bool isSecure = false, bool isStringList = false});

  Future<bool> setValue(String key, dynamic value);

  Future<dynamic> setSecureValue(String key, String value);

  Future<bool> remove(String key, {bool isSecure = false});

  Future<bool> cleanUp();

  Future<void> cleanUpSecure();
}
