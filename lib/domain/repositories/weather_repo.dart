import '../models/models.dart';

abstract class WeatherRepo {
  Future<CurrentWeatherResponse> fetchWeather(String apiKey, LatLng latLng);
}
