enum SharedPrefKey {
  initialized('initialized', false),
  latestCityName('latest_city_name', false),
  latestPosition('latest_position', true);

  const SharedPrefKey(this.key, this.isSecure);

  final String key;
  final bool isSecure;
}
