import 'package:get/get.dart';

enum WeatherCondition {
  clearSky('01', 800, 800),
  fewClouds('02', 801, 801),
  scatteredClouds('03', 802, 802),
  brokenClouds('04', 803, 804),
  showerRain('09', 300, 399),
  rain('10', 500, 599),
  thunderstorm('11', 200, 299),
  snow('13', 600, 699),
  mist('50', 700, 799);

  const WeatherCondition(this.code, this.minId, this.maxId);

  final String code;
  final int minId;
  final int maxId;

  static WeatherCondition? fromId(int? id) {
    if (id == null) return null;
    final result = values.firstWhereOrNull((e) => id >= e.minId && id <= e.maxId);
    return result;
  }
}
