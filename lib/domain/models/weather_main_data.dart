import 'package:json_annotation/json_annotation.dart';

part 'weather_main_data.g.dart';

@JsonSerializable(explicitToJson: true)
class WeatherMainData {
  const WeatherMainData({this.feelsLike, this.humidity, this.temp, this.tempMax, this.tempMin});

  final double? temp;
  final double? humidity;

  @JsonKey(name: 'feels_like')
  final double? feelsLike;
  @JsonKey(name: 'temp_min')
  final double? tempMin;
  @JsonKey(name: 'temp_max')
  final double? tempMax;

  factory WeatherMainData.fromJson(Map<String, dynamic> json) => _$WeatherMainDataFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherMainDataToJson(this);
}
