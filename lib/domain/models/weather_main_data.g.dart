// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_main_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherMainData _$WeatherMainDataFromJson(Map<String, dynamic> json) =>
    WeatherMainData(
      feelsLike: (json['feels_like'] as num?)?.toDouble(),
      humidity: (json['humidity'] as num?)?.toDouble(),
      temp: (json['temp'] as num?)?.toDouble(),
      tempMax: (json['temp_max'] as num?)?.toDouble(),
      tempMin: (json['temp_min'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$WeatherMainDataToJson(WeatherMainData instance) =>
    <String, dynamic>{
      'temp': instance.temp,
      'humidity': instance.humidity,
      'feels_like': instance.feelsLike,
      'temp_min': instance.tempMin,
      'temp_max': instance.tempMax,
    };
