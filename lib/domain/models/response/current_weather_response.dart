import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../sys.dart';
import '../weather.dart';
import '../weather_main_data.dart';

part 'current_weather_response.g.dart';

@JsonSerializable(explicitToJson: true)
class CurrentWeatherResponse {
  const CurrentWeatherResponse({
    this.isCurrentLocation = false,
    this.isSuccess = true,
    this.statusCode = 200,
    this.exception,
    this.id,
    this.name,
    this.weather = const [],
    this.sys,
    this.main,
  });

  final int? id;
  final String? name;
  final List<Weather> weather;
  final Sys? sys;
  final WeatherMainData? main;

  @JsonKey(name: 'cod')
  final int statusCode;

  /// ignored
  @JsonKey(includeFromJson: false, includeToJson: false)
  final bool isCurrentLocation;
  @JsonKey(includeFromJson: false, includeToJson: false)
  final bool isSuccess;
  @JsonKey(includeFromJson: false, includeToJson: false)
  final String? exception;

  DateTime? getSunsetTime() {
    final value = sys?.sunset;
    if (value != null) {
      return DateTime.fromMillisecondsSinceEpoch(value * 1000);
    }
    return null;
  }

  DateTime? getSunriseTime() {
    final value = sys?.sunrise;
    if (value != null) {
      return DateTime.fromMillisecondsSinceEpoch(value * 1000);
    }
    return null;
  }

  factory CurrentWeatherResponse.fromJson(Map<String, dynamic> json) => _$CurrentWeatherResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentWeatherResponseToJson(this);

  factory CurrentWeatherResponse.fromException(ex) {
    int errorCode = 500;

    if (ex is DioException) {
      errorCode = ex.response?.statusCode ?? 500;
    }

    return CurrentWeatherResponse(
      statusCode: errorCode,
      isSuccess: false,
      exception: ex?.toString(),
    );
  }
}
