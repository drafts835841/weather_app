// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'current_weather_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrentWeatherResponse _$CurrentWeatherResponseFromJson(
        Map<String, dynamic> json) =>
    CurrentWeatherResponse(
      statusCode: json['cod'] as int? ?? 200,
      id: json['id'] as int?,
      name: json['name'] as String?,
      weather: (json['weather'] as List<dynamic>?)
              ?.map((e) => Weather.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      sys: json['sys'] == null
          ? null
          : Sys.fromJson(json['sys'] as Map<String, dynamic>),
      main: json['main'] == null
          ? null
          : WeatherMainData.fromJson(json['main'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CurrentWeatherResponseToJson(
        CurrentWeatherResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'weather': instance.weather.map((e) => e.toJson()).toList(),
      'sys': instance.sys?.toJson(),
      'main': instance.main?.toJson(),
      'cod': instance.statusCode,
    };
