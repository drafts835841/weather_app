import 'package:json_annotation/json_annotation.dart';

part 'lat_lng.g.dart';

@JsonSerializable(explicitToJson: true)
class LatLng {
  const LatLng(this.lat, this.lon);

  final double lon;
  final double lat;

  factory LatLng.fromJson(Map<String, dynamic> json) => _$LatLngFromJson(json);

  Map<String, dynamic> toJson() => _$LatLngToJson(this);

  static LatLng? fromString(String value) {
    final args = value.split(',');

    if (args.length == 2) {
      final lat = double.tryParse(args[0].trim());
      final lng = double.tryParse(args[1].trim());
      if (lat != null && lng != null) {
        return LatLng(lat, lng);
      }
    }

    return null;
  }

  @override
  String toString() => '$lat,$lon';
}
