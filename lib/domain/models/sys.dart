import 'package:json_annotation/json_annotation.dart';

part 'sys.g.dart';

@JsonSerializable(explicitToJson: true)
class Sys {
  const Sys({this.sunrise, this.sunset});

  final int? sunrise;
  final int? sunset;

  factory Sys.fromJson(Map<String, dynamic> json) => _$SysFromJson(json);

  Map<String, dynamic> toJson() => _$SysToJson(this);
}
