import 'package:json_annotation/json_annotation.dart';

import 'lat_lng.dart';

part 'city.g.dart';

@JsonSerializable(explicitToJson: true)
class City {
  const City({this.lat, this.lng, this.city});

  final String? city;
  final String? lat;
  final String? lng;

  LatLng? get latLng {
    final latValue = double.tryParse(lat ?? '');
    final lngValue = double.tryParse(lng ?? '');

    if (latValue != null && lngValue != null) return LatLng(latValue, lngValue);
    return null;
  }

  factory City.fromJson(Map<String, dynamic> json) => _$CityFromJson(json);

  Map<String, dynamic> toJson() => _$CityToJson(this);
}
