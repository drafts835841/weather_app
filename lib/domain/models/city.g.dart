// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

City _$CityFromJson(Map<String, dynamic> json) => City(
      lat: json['lat'] as String?,
      lng: json['lng'] as String?,
      city: json['city'] as String?,
    );

Map<String, dynamic> _$CityToJson(City instance) => <String, dynamic>{
      'city': instance.city,
      'lat': instance.lat,
      'lng': instance.lng,
    };
