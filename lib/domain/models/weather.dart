import 'package:json_annotation/json_annotation.dart';

import 'enums/enums.dart';

part 'weather.g.dart';

@JsonSerializable(explicitToJson: true)
class Weather {
  const Weather({this.id, this.icon});

  final int? id;
  final String? icon;

  factory Weather.fromJson(Map<String, dynamic> json) => _$WeatherFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherToJson(this);

  WeatherCondition? get condition => WeatherCondition.fromId(id);
}
