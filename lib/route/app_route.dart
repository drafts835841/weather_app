import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../presentation/features/features.dart';

enum AppRoute {
  main('/main'),
  placeSearching('/placeSearching');

  const AppRoute(this.path);

  final String path;

  Widget get page {
    switch (this) {
      case AppRoute.main:
        return MainPage();
      case AppRoute.placeSearching:
        return PlaceSearchingPage();

      default:
        return const SizedBox.shrink();
    }
  }

  List<Bindings> get bindings {
    switch (this) {
      case AppRoute.main:
        return MainPage.bindings;
      case AppRoute.placeSearching:
        return PlaceSearchingPage.bindings;

      default:
        return const [];
    }
  }
}

final appPages = _pages;

List<GetPage> get _pages {
  var fullscreenDialog = false;
  Transition? transition;

  if (GetPlatform.isAndroid) {
    fullscreenDialog = true;
    transition = Transition.rightToLeft;
  }

  final list = AppRoute.values
      .map((route) => GetPage(
            fullscreenDialog: fullscreenDialog,
            transition: transition,
            name: route.path,
            page: () => route.page,
            bindings: route.bindings,
          ))
      .toList();

  return list;
}
