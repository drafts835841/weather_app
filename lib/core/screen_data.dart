enum ScreenState { ideal, loading, refresh, loadMore, success, error }

class ScreenData<T> {
  T? data;
  String? message;
  List<String?>? messages;
  ScreenState screenState;

  ScreenData({
    this.data,
    this.message,
    this.messages,
    this.screenState = ScreenState.ideal,
  });
}
