import 'dart:async';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';

import 'app/app.dart';
import 'di/injection.dart';
import 'utils/utils.dart';

void main() {
  runZonedGuarded<void>(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      await configureDependencies();
      GeolocatorUtil.getInstance();
      runApp(const MyApp());
    },
    (error, stack) {
      developer.log(error.toString(), error: error, stackTrace: stack);
    },
  );
}
