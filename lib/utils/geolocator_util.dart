import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';

import '../app/extensions/extensions.dart';
import '../domain/domain.dart';

class GeolocatorUtil {
  static GeolocatorUtil? _instance;

  static GeolocatorUtil getInstance() {
    GeolocatorUtil instance = _instance ?? GeolocatorUtil._({});
    _instance ??= instance;
    _instance?.init();
    return instance;
  }

  static Future<Position> get determinePosition async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }

  GeolocatorUtil._(this.map);

  Map<String, List<City>> map;

  List<City> get cities {
    return map.values.expand((e) => e).toList();
  }

  void init() async {
    if (map.isEmpty) {
      final result = await rootBundle.loadString('assets/json/vn.json');
      final json = jsonDecode(result);

      if (json is List) {
        for (var item in json) {
          final value = City.fromJson(item);
          final key = value.city?.toNonAccentVietnamese().toLowerCase();

          if (key != null) {
            if (map.containsKey(key)) {
              map[key]!.add(value);
            } else {
              map[key] = [value];
            }
          }
        }
      }
    }
  }
}
