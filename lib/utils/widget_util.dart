import 'package:flutter/widgets.dart';

class WidgetUtil {
  static const emptyBox = SizedBox.shrink();

  static Widget space({double? width, double? height}) {
    if (width != null && width < 0) return const SizedBox.shrink();
    if (height != null && height < 0) return const SizedBox.shrink();
    return SizedBox(width: width, height: height);
  }

  static Widget fullBox({Widget? child}) => SizedBox(width: double.infinity, height: double.infinity, child: child);
}
