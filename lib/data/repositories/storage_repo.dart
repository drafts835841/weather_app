import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../domain/domain.dart';

@Injectable(as: StorageRepo)
class StorageRepoImpl implements StorageRepo {
  StorageRepoImpl() {
    _secureStorage = const FlutterSecureStorage();
  }

  late final FlutterSecureStorage _secureStorage;

  @override
  Future<Map<String, String>> readAll() async {
    final prefs = await SharedPreferences.getInstance();
    Map<String, String> data = await _secureStorage.readAll();
    prefs.getKeys().forEach((element) {
      data[element] = prefs.get(element)?.toString() ?? '';
    });
    return data;
  }

  @override
  Future<dynamic> getValue(String key, {bool isSecure = false, bool isStringList = false}) async {
    if (isSecure) {
      return await _secureStorage.read(key: key);
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (isStringList) {
      return prefs.getStringList(key);
    }

    return prefs.get(key);
  }

  @override
  Future<bool> setValue(String key, dynamic value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (value is int) {
      return prefs.setInt(key, value);
    } else if (value is double) {
      return prefs.setDouble(key, value);
    } else if (value is bool) {
      return prefs.setBool(key, value);
    } else if (value is String) {
      return prefs.setString(key, value);
    } else if (value is List<String>) {
      return prefs.setStringList(key, value);
    } else {
      throw Exception("Value type is not supported");
    }
  }

  @override
  Future<dynamic> setSecureValue(String key, String value) async {
    return await _secureStorage.write(
      key: key,
      value: value,
    );
  }

  @override
  Future<bool> remove(String key, {bool isSecure = false}) async {
    if (isSecure) await _secureStorage.delete(key: key);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.remove(key);
  }

  @override
  Future<bool> cleanUp() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await _secureStorage.deleteAll();
    return await prefs.clear();
  }

  @override
  Future<void> cleanUpSecure() async {
    await _secureStorage.deleteAll();
  }
}
