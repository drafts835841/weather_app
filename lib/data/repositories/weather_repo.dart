import 'package:injectable/injectable.dart';

import '../../domain/domain.dart';
import '../datasources/datasources.dart';

@Injectable(as: WeatherRepo)
class WeatherRepoImpl implements WeatherRepo {
  const WeatherRepoImpl(this._service);

  final WeatherService _service;

  @override
  Future<CurrentWeatherResponse> fetchWeather(String apiKey, LatLng latLng) async {
    try {
      final response = await _service.fetchCurrentWeather(latLng.lat, latLng.lon, apiKey);
      return response;
    } catch (ex) {
      return CurrentWeatherResponse.fromException(ex);
    }
  }
}
