import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

import '../../../domain/domain.dart';
import 'api_clients/weather_api_client.dart';

part 'weather_service.g.dart';

@lazySingleton
@RestApi()
abstract class WeatherService {
  @factoryMethod
  factory WeatherService(WeatherApiClient client) => _WeatherService(client.dio, baseUrl: client.baseUrl);

  @GET('/data/2.5/weather?lat={lat}&lon={lon}&appid={appid}&units=metric')
  Future<CurrentWeatherResponse> fetchCurrentWeather(
    @Path('lat') double lat,
    @Path('lon') double lon,
    @Path('appid') String apiKey,
  );
}
