import 'package:dio/dio.dart';

abstract class BaseApiClient {
  Dio getDio({
    required String endpoint,
    int? timeOut,
    String? contentType,
  }) {
    Dio dio = Dio();

    // API end point
    dio.options.baseUrl = endpoint;

    // Set time out
    dio.options
      ..connectTimeout = Duration(milliseconds: timeOut ?? 20000)
      ..receiveTimeout = Duration(milliseconds: timeOut ?? 20000)
      ..contentType = contentType ?? 'application/json';

    dio.interceptors.add(defaultRequestInterceptor);

    return dio;
  }

  Map<String, dynamic>? getHeaders();

  Future<Map<String, dynamic>?>? getHeadersAsync();

  Interceptor get defaultRequestInterceptor {
    return InterceptorsWrapper(
      onRequest: (options, handler) async {
        final header = getHeaders();

        var headerAsync = await getHeadersAsync();

        if (header != null) {
          options.headers.addAll(header);
        }

        if (headerAsync != null) {
          options.headers.addAll(headerAsync);
        }

        return handler.next(options);
      },
    );
  }
}
