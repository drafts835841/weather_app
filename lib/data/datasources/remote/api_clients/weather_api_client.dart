import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

import 'base_api_client.dart';

@lazySingleton
class WeatherApiClient extends BaseApiClient {
  WeatherApiClient() {
    _dio = getDio(
      endpoint: 'https://api.openweathermap.org',
      timeOut: 300000,
    );
  }

  late final Dio _dio;

  Dio get dio => _dio;

  String get baseUrl => _dio.options.baseUrl;

  @override
  Map<String, dynamic>? getHeaders() => null;

  @override
  Future<Map<String, dynamic>?>? getHeadersAsync() => Future.value(null);
}
