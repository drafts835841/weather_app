import 'package:flutter/material.dart';

import '../../app/constants/constants.dart';
import '../../app/extensions/extensions.dart';
import '../../utils/utils.dart';

class AppEmptyWidget extends StatelessWidget {
  final String? emptyMessage;
  final VoidCallback? onRefresh;
  final String? retryLabel;
  final bool showRetryButton;

  const AppEmptyWidget({
    super.key,
    this.emptyMessage,
    this.onRefresh,
    this.retryLabel,
    this.showRetryButton = true,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            emptyMessage ?? context.l10n.emptyDataMessage,
            textAlign: TextAlign.center,
          ),
          WidgetUtil.space(height: Dimens.size16.w),
          if (showRetryButton)
            TextButton(
              onPressed: onRefresh,
              child: Text(retryLabel ?? context.l10n.retry),
            )
        ],
      ),
    );
  }
}
