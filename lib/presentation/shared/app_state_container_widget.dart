import 'package:flutter/material.dart';

import '../../core/core.dart';
import 'app_empty_widget.dart';
import 'app_error_widget.dart';
import 'app_loading_widget.dart';

class AppStateContainerWidget extends StatelessWidget {
  const AppStateContainerWidget({
    super.key,
    required this.data,
    required this.successBuilder,
    this.idealBuilder,
    this.loadingBuilder,
    this.errorBuilder,
    this.emptyBuilder,
    this.isShowEmptyState = false,
    this.onRetry,
    this.emptyMessage,
    this.loadingMessage,
  });

  final ScreenData? data;
  final Widget Function(BuildContext context)? successBuilder;
  final Widget Function(BuildContext context)? idealBuilder;
  final Widget Function(BuildContext context)? loadingBuilder;
  final Widget Function(BuildContext context)? errorBuilder;
  final Widget Function(BuildContext context)? emptyBuilder;
  final bool isShowEmptyState;
  final VoidCallback? onRetry;
  final String? emptyMessage;
  final String? loadingMessage;

  @override
  Widget build(BuildContext context) {
    Widget? widget;

    switch (data?.screenState) {
      case ScreenState.ideal:
        widget = idealBuilder?.call(context);
        break;

      case ScreenState.loading:
        widget = loadingBuilder?.call(context) ?? AppLoadingWidget(loadingMessage: loadingMessage);
        break;

      case ScreenState.success:
        if (isShowEmptyState == true) {
          widget = emptyBuilder?.call(context) ?? AppEmptyWidget(emptyMessage: emptyMessage, onRefresh: onRetry);
        } else {
          widget = successBuilder?.call(context);
        }
        break;

      case ScreenState.error:
        widget = errorBuilder?.call(context) ?? AppErrorWidget(errorMessage: data?.message, onRetry: onRetry);
        break;

      case ScreenState.refresh:
      case ScreenState.loadMore:
        widget = successBuilder?.call(context);
        break;

      default:
        break;
    }

    return SizedBox(child: widget);
  }
}
