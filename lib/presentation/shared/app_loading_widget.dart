import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../../app/constants/constants.dart';
import '../../app/extensions/extensions.dart';

class AppLoadingWidget extends StatelessWidget {
  final String? loadingMessage;

  const AppLoadingWidget({
    Key? key,
    this.loadingMessage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SpinKitCubeGrid(color: context.theme.colorScheme.secondary, size: Dimens.size30.w),
            Text(loadingMessage ?? ''),
          ],
        ),
      ),
    );
  }
}
