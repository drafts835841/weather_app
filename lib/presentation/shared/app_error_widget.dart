import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/constants/constants.dart';
import '../../app/extensions/extensions.dart';
import '../../utils/utils.dart';

class AppErrorWidget extends StatelessWidget {
  final VoidCallback? onRetry;
  final String? errorMessage;

  const AppErrorWidget({
    super.key,
    this.onRetry,
    this.errorMessage,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(errorMessage?.tr ?? ''),
          WidgetUtil.space(height: Dimens.size8.w),
          TextButton(
            onPressed: onRetry,
            child: Text(context.l10n.retry),
          ),
        ],
      ),
    );
  }
}
