import 'dart:async';

import 'package:get/get.dart';
import 'package:get_it/get_it.dart';

import '../../../app/extensions/extensions.dart';
import '../../../core/core.dart';
import '../../../domain/domain.dart';
import '../../../utils/utils.dart';

class PlaceSearchingPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlaceSearchingPageController>(
      () => PlaceSearchingPageController(
        GetIt.instance.get<StorageRepo>(),
      ),
    );
  }
}

class PlaceSearchingPageController extends GetxController {
  PlaceSearchingPageController(StorageRepo storageRepo) {
    _detectLocationUsecase = DetectLocationUsecase(storageRepo);
  }

  late final DetectLocationUsecase _detectLocationUsecase;

  final screenData = ScreenData<List<City>>().obs;
  final _citiesMap = GeolocatorUtil.getInstance().map;
  String _searchText = '';
  Timer? _searchTimer;

  List<City> get _cities {
    return _citiesMap.values.expand((e) => e).toList();
  }

  Future<bool> get validToPop async {
    final value = await _detectLocationUsecase.defaultLatLng;
    return value != null;
  }

  @override
  void onReady() {
    super.onReady();
    screenData
      ..value.data = _cities
      ..value.screenState = ScreenState.success
      ..refresh();
  }

  @override
  void dispose() {
    _searchTimer?.cancel();
    super.dispose();
  }

  void searchingPlace(String text) {
    _searchText = text.toNonAccentVietnamese().toLowerCase();
    _searchTimer = Timer(const Duration(milliseconds: 300), _searchPlace);
  }

  void _searchPlace() {
    final keys = _citiesMap.keys.where((e) => e.contains(_searchText)).toList();

    final results = <City>[];
    for (var key in keys) {
      results.addAll(_citiesMap[key]?.toList() ?? []);
    }

    screenData
      ..value.data = results
      ..value.screenState = ScreenState.success
      ..refresh();
  }

  Future<bool> selectCity(City city) async {
    final latLng = city.latLng;

    if (latLng != null) {
      await Future.wait([
        _detectLocationUsecase.setDefaultLatLng(latLng),
        _detectLocationUsecase.setDefaultCityName(city.city),
      ]);
      return true;
    }

    return false;
  }
}
