import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../../app/constants/constants.dart';
import '../../../app/extensions/extensions.dart';
import '../../../domain/domain.dart';
import '../../../utils/utils.dart';
import '../../shared/shared.dart';
import 'place_searching_page_controller.dart';

class PlaceSearchingPage extends StatelessWidget {
  static List<Bindings> get bindings => [PlaceSearchingPageBinding()];

  PlaceSearchingPage({super.key});

  final _controller = Get.find<PlaceSearchingPageController>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onWillPop(context),
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          appBar: AppBar(
            title: CupertinoTextField(
              placeholder: context.l10n.searchPlaces,
              placeholderStyle: context.textTheme.bodyLarge?.copyWith(
                color: context.theme.disabledColor,
              ),
              style: context.textTheme.bodyLarge,
              onChanged: (text) => _controller.searchingPlace(text),
            ),
          ),
          body: WidgetUtil.fullBox(child: Obx(() {
            final data = _controller.screenData.value;
            return AppStateContainerWidget(
              data: data,
              successBuilder: (context) => Obx(() => _successBuilder(context, _controller.screenData.value.data)),
            );
          })),
        ),
      ),
    );
  }

  Widget _successBuilder(BuildContext context, List<City>? data) {
    return ListView.separated(
      physics: const ClampingScrollPhysics(),
      padding: EdgeInsets.zero,
      itemBuilder: (_, index) => InkWell(
        onTap: () => _selectCity(data[index]),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: Dimens.size8.w, horizontal: Dimens.size10.w),
          child: Row(
            children: [
              Expanded(child: Text(data![index].city ?? '-')),
              Icon(
                Icons.arrow_forward_ios_rounded,
                size: Dimens.size16.w,
                color: context.textTheme.bodyMedium?.color,
              )
            ],
          ),
        ),
      ),
      separatorBuilder: (_, __) => Divider(height: Dimens.size1.w),
      itemCount: data?.length ?? 0,
    );
  }

  Future<bool> _onWillPop(BuildContext context) async {
    if (await _controller.validToPop) {
      return true;
    }

    if (Platform.isAndroid) {
      SystemNavigator.pop();
    } else if (Platform.isIOS) {
      exit(0);
    } else {
      return true;
    }

    return false;
  }

  void _selectCity(City city) async {
    final result = await _controller.selectCity(city);
    if (result) {
      Get.back(result: true);
    }
  }
}
