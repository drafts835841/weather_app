import 'package:get/get.dart';
import 'package:get_it/get_it.dart';

import '../../../core/core.dart';
import '../../../domain/domain.dart';
import '../../../route/app_route.dart';
import '../../../utils/utils.dart';

class MainPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MainPageController>(
      () => MainPageController(
        GetIt.instance.get<StorageRepo>(),
        GetIt.instance.get<WeatherRepo>(),
      ),
    );
  }
}

class MainPageController extends GetxController {
  MainPageController(StorageRepo storageRepo, WeatherRepo weatherRepo) {
    _splashUsecase = SplashUsecase(storageRepo);
    _detectLocationUsecase = DetectLocationUsecase(storageRepo);
    _weatherUsecase = WeatherUsecase(weatherRepo);
  }

  late final SplashUsecase _splashUsecase;
  late final DetectLocationUsecase _detectLocationUsecase;
  late final WeatherUsecase _weatherUsecase;

  final screenData = ScreenData<CurrentWeatherResponse>().obs;
  final cityName = Rxn<String>();

  WeatherCondition? get weatherCondition => WeatherCondition.fromId(screenData.value.data?.weather.firstOrNull?.id);

  @override
  void onInit() {
    super.onInit();
    _init();
  }

  Future<void> _init() async {
    await _splashUsecase.init();
    _determinePosition();
  }

  void _determinePosition() async {
    screenData
      ..value.screenState = ScreenState.loading
      ..refresh();

    final latLng = await _getLatestLocation();
    final localCityName = await _getLocalCityName();

    if (latLng != null) {
      _fetchWeather(latLng, localCityName: localCityName);
    } else {
      GeolocatorUtil.determinePosition
          .then((position) => _setDefaultLocation(LatLng(position.latitude, position.longitude)))
          .onError((error, stackTrace) => _handleUnavailableDefaultLocation());
    }
  }

  void _setDefaultLocation(LatLng latLng) {
    _detectLocationUsecase.setDefaultLatLng(latLng);
    _fetchWeather(latLng);
  }

  Future<LatLng?> _getLatestLocation() async {
    final latLng = await _detectLocationUsecase.defaultLatLng;
    return latLng;
  }

  Future<String?> _getLocalCityName() => _detectLocationUsecase.defaultCityName;

  void _handleUnavailableDefaultLocation() async {
    final result = await Get.toNamed(AppRoute.placeSearching.path);

    if (result != null && result is bool && result) {
      onRetry(getLocalCityName: true);
    }
  }

  void _fetchWeather(LatLng latLng, {String? localCityName}) async {
    final response = await _weatherUsecase.fetchData(latLng);

    if (response != null) {
      cityName
        ..value = localCityName ?? response.name
        ..refresh();

      screenData
        ..value.screenState = response.isSuccess ? ScreenState.success : ScreenState.error
        ..value.data = response
        ..refresh();

      return;
    }

    screenData
      ..value.screenState = ScreenState.error
      ..refresh();
  }

  void onRetry({bool getLocalCityName = false}) async {
    final latLng = await _getLatestLocation();
    String? localCityName = await _getLocalCityName();

    if (latLng != null) {
      _fetchWeather(latLng, localCityName: localCityName);
    } else {
      _determinePosition();
    }
  }
}
