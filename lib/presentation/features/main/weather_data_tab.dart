import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/constants/constants.dart';
import '../../../app/extensions/extensions.dart';
import '../../../app/resources.dart';
import '../../../domain/domain.dart';
import '../../../utils/utils.dart';
import 'weather_condition_extension.dart';

class WeatherDataTab extends StatelessWidget {
  const WeatherDataTab({
    super.key,
    this.condition,
    this.temp,
    this.sunrise,
    this.sunset,
    this.tempMin,
    this.tempMax,
  });

  final WeatherCondition? condition;
  final int? temp;
  final int? tempMin;
  final int? tempMax;
  final DateTime? sunrise;
  final DateTime? sunset;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(child: Center(child: _buildWeatherCondition(context, condition, temp))),
        SizedBox(
          height: Dimens.size80.w,
          child: Row(
            children: [
              const Spacer(),
              _buildAnotherInfo1(
                context,
                icon: Assets.images.icoSunrise,
                label: context.l10n.sunrise,
                value: sunrise?.toStringFormat(pattern: 'HH:mm') ?? '-',
              ),
              WidgetUtil.space(width: Dimens.size49.w),
              _buildAnotherInfo1(
                context,
                icon: Assets.images.icoSunset,
                label: context.l10n.sunset,
                value: sunset?.toStringFormat(pattern: 'HH:mm') ?? '-',
              ),
              const Spacer(),
            ],
          ),
        ),
        ...(tempMax != null && tempMin != null)
            ? [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: Dimens.size20.w),
                  child: const Divider(),
                ),
                SizedBox(
                  height: Dimens.size80.w,
                  child: Row(
                    children: [
                      const Spacer(),
                      _buildAnotherInfo1(
                        context,
                        icon: Assets.images.icoTempMin,
                        label: context.l10n.tempMin,
                        value: '$tempMin°C',
                      ),
                      WidgetUtil.space(width: Dimens.size49.w),
                      _buildAnotherInfo1(
                        context,
                        icon: Assets.images.icoTempMax,
                        label: context.l10n.tempMax,
                        value: '$tempMax°C',
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
              ]
            : [],
      ],
    );
  }

  Widget _buildWeatherCondition(BuildContext context, WeatherCondition? condition, int? temp) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        condition == null
            ? Center(
                child: Text(
                  '--',
                  style: context.textTheme.displayLarge,
                  textAlign: TextAlign.center,
                ),
              )
            : condition.image.image(width: Dimens.size155.w, height: Dimens.size155.w),
        if (condition != null) _buildTemp(context, temp),
      ],
    );
  }

  Widget _buildTemp(BuildContext context, int? temp) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        style: context.theme.textTheme.titleLarge,
        children: [
          WidgetSpan(
            alignment: PlaceholderAlignment.top,
            child: Text(
              temp?.toString() ?? '-',
              style: context.theme.textTheme.displayLarge,
            ),
          ),
          WidgetSpan(
            alignment: PlaceholderAlignment.top,
            child: Text(
              '°C',
              style: context.theme.textTheme.titleLarge,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAnotherInfo1(
    BuildContext context, {
    required AssetGenImage icon,
    required String label,
    required String value,
  }) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        icon.image(
          width: Dimens.size49.w,
          height: Dimens.size49.w,
        ),
        WidgetUtil.space(width: Dimens.size4.w),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(label),
            Text(value, style: context.textTheme.titleMedium),
          ],
        ),
      ],
    );
  }
}
