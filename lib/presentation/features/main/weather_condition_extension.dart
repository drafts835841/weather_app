import '../../../app/resources.dart';
import '../../../domain/domain.dart';

extension WeatherConditionExt on WeatherCondition {
  AssetGenImage get image {
    switch (this) {
      case WeatherCondition.clearSky:
        return Assets.images.ico01;
      case WeatherCondition.fewClouds:
        return Assets.images.ico02;
      case WeatherCondition.scatteredClouds:
        return Assets.images.ico03;
      case WeatherCondition.brokenClouds:
        return Assets.images.ico04;
      case WeatherCondition.showerRain:
        return Assets.images.ico09;
      case WeatherCondition.rain:
        return Assets.images.ico10;
      case WeatherCondition.thunderstorm:
        return Assets.images.ico11;
      case WeatherCondition.snow:
        return Assets.images.ico13;
      case WeatherCondition.mist:
        return Assets.images.ico50;
    }
  }
}
