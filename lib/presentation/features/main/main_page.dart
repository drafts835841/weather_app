import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../domain/domain.dart';
import '../../../route/app_route.dart';
import '../../../utils/utils.dart';
import '../../shared/shared.dart';
import 'main_page_controller.dart';
import 'weather_data_tab.dart';

class MainPage extends StatelessWidget {
  static List<Bindings> get bindings => [MainPageBinding()];

  MainPage({super.key});

  final _controller = Get.find<MainPageController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: _placeSearching,
            icon: const Icon(Icons.search_rounded),
          )
        ],
        title: Obx(
          () => Text(
            _controller.cityName.value ?? '-',
            textAlign: TextAlign.center,
          ),
        ),
      ),
      body: WidgetUtil.fullBox(child: Obx(() {
        final data = _controller.screenData.value;
        return AppStateContainerWidget(
          data: data,
          successBuilder: (context) => Obx(() => _successBuilder(context, _controller.screenData.value.data)),
          onRetry: () => _controller.onRetry(),
        );
      })),
    );
  }

  Widget _successBuilder(BuildContext context, CurrentWeatherResponse? data) {
    if (data == null) return const SizedBox.shrink();

    return WeatherDataTab(
      condition: data.weather.firstOrNull?.condition,
      temp: data.main?.temp?.round(),
      sunrise: data.getSunriseTime(),
      sunset: data.getSunsetTime(),
      tempMin: data.main?.tempMin?.round(),
      tempMax: data.main?.tempMax?.round(),
    );
  }

  void _placeSearching() async {
    final result = await Get.toNamed(AppRoute.placeSearching.path);

    if (result != null && result is bool && result) {
      _controller.onRetry(getLocalCityName: true);
    }
  }
}
