extension StringExt on String {
  String toNonAccentVietnamese() {
    String str = this;
    str = str.replaceAll(RegExp(r'A|Á|À|Ã|Ạ|Â|Ấ|Ầ|Ẫ|Ậ|Ă|Ắ|Ằ|Ẵ|Ặ'), "A");
    str = str.replaceAll(RegExp(r'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ'), "a");
    str = str.replaceAll(RegExp(r'E|É|È|Ẽ|Ẹ|Ê|Ế|Ề|Ễ|Ệ/'), "E");
    str = str.replaceAll(RegExp(r'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ'), "e");
    str = str.replaceAll(RegExp(r'I|Í|Ì|Ĩ|Ị'), "I");
    str = str.replaceAll(RegExp(r'ì|í|ị|ỉ|ĩ'), "i");
    str = str.replaceAll(RegExp(r'O|Ó|Ò|Õ|Ọ|Ô|Ố|Ồ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ỡ|Ợ'), "O");
    str = str.replaceAll(RegExp(r'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ'), "o");
    str = str.replaceAll(RegExp(r'U|Ú|Ù|Ũ|Ụ|Ư|Ứ|Ừ|Ữ|Ự'), "U");
    str = str.replaceAll(RegExp(r'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ'), "u");
    str = str.replaceAll(RegExp(r'Y|Ý|Ỳ|Ỹ|Ỵ'), "Y");
    str = str.replaceAll(RegExp(r'ỳ|ý|ỵ|ỷ|ỹ'), "y");
    str = str.replaceAll(RegExp(r'Đ'), "D");
    str = str.replaceAll(RegExp(r'đ'), "d");
    return str;
  }
}
