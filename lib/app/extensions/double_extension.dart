import 'package:flutter_screenutil/flutter_screenutil.dart';

extension DoubleExt on double {
  double get w => ScreenUtil().setWidth(this);

  double get h => ScreenUtil().setHeight(this);
}
