import 'package:intl/intl.dart';

extension DateTimeExtension on DateTime {
  String toStringFormat({String? pattern}) {
    return DateFormat(pattern).format(this);
  }
}
