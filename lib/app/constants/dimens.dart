class Dimens {
  static const size1 = 1.0;
  static const size4 = 4.0;
  static const size5 = 5.0;
  static const size8 = 8.0;
  static const size10 = 10.0;
  static const size16 = 16.0;
  static const size20 = 20.0;
  static const size28 = 28.0;
  static const size30 = 30.0;
  static const size49 = 49.0;
  static const size80 = 80.0;
  static const size155 = 155.0;
  static const size185 = 185.0;
}

class Percentages {
  static const percent20 = 0.2;
  static const percent50 = 0.5;
}
