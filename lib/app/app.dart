import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../route/app_route.dart' as route;
import 'extensions/extensions.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      minTextAdapt: true,
      child: GetMaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData.dark(useMaterial3: true),
        localizationsDelegates: context.localizationsDelegates,
        supportedLocales: context.supportedLocales,
        locale: const Locale('vi'),
        initialRoute: route.AppRoute.main.path,
        getPages: route.appPages,
      ),
    );
  }
}
