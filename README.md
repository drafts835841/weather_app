# HOMEBASE's Flutter Assignment: Weather App

## Documentation

- [Install Flutter](https://flutter.dev/get-started/)
- [Flutter documentation](https://flutter.dev/docs)

## Installation

- Dart 3.1.3
- Flutter 3.13.6
- CocoaPods 1.13.0

## Environment

**iOS**
- iOS 13+

**Android**
- Android 7+
    - minSdkVersion 24
- targetSdkVersion 33

## Getting Started

### Setup

```shell script
$ dependencies: flutter pub get
```
### How to add localizations
1. Edit [*.arb]() files into lib/l10n.
2. Run generate the `flutter gen-l10n`

### When update asset folder (svgs, images, font), chopper, router should run script:
```shell script
$ dart run build_runner build --delete-conflicting-outputs
```
